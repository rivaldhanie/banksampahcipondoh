-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 30 Agu 2022 pada 11.38
-- Versi server: 10.4.24-MariaDB
-- Versi PHP: 7.4.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `banksampah`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_fcm`
--

CREATE TABLE `tb_fcm` (
  `id_fcm` int(10) NOT NULL,
  `id_nasabah` int(12) NOT NULL,
  `fcm` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_fcm`
--

INSERT INTO `tb_fcm` (`id_fcm`, `id_nasabah`, `fcm`) VALUES
(1, 12310001, 'dfrgTGY0T8WLy2iubDzaa3:APA91bGICDrDqUH2clUV4bNzc2-XGOjluOaQ8UxbMDrZK44rMT8BXQlTwHp5JBa8cs1DEbAd_e9rJ9WoD7pUOrqyjPpa1g5ws1NXDgPEyw6l9xsGV-OHWbPpQlGyt9QlVZk2Aa_MWZsV');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_nasabah`
--

CREATE TABLE `tb_nasabah` (
  `id_nasabah` int(12) NOT NULL,
  `pass_nasabah` int(8) NOT NULL,
  `nama_nasabah` varchar(100) NOT NULL,
  `alamat_nasabah` text NOT NULL,
  `telp_nasabah` varchar(13) NOT NULL,
  `id_login` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_nasabah`
--

INSERT INTO `tb_nasabah` (`id_nasabah`, `pass_nasabah`, `nama_nasabah`, `alamat_nasabah`, `telp_nasabah`, `id_login`) VALUES
(12310001, 123, 'Rival Achmadani', 'Cipondoh Makmur', '081281442747', 101),
(12310002, 123, 'Rafid Annurrifa', 'Serpong', '0876464664', 102),
(12310003, 123, 'Muhamad Yusup', 'Alam Sutera', '08784661626', 102),
(12310004, 123, 'Ali Alfaris', 'Serpong', '0215416484', 102),
(12310005, 123, 'Anton', 'Serpong', '0245464854', 102);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_picture`
--

CREATE TABLE `tb_picture` (
  `id_picture` int(11) NOT NULL,
  `path_picture` varchar(100) NOT NULL,
  `ket_picture` varchar(100) NOT NULL,
  `id_nasabah` int(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_picture`
--

INSERT INTO `tb_picture` (`id_picture`, `path_picture`, `ket_picture`, `id_nasabah`) VALUES
(33100004, 'picture/profile_12310001.png', 'profile', 12310001);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_record`
--

CREATE TABLE `tb_record` (
  `id_record` int(11) NOT NULL,
  `id_nasabah` int(11) NOT NULL,
  `ket_record` text NOT NULL,
  `jumlah_record` int(11) NOT NULL,
  `tanggal_record` varchar(30) NOT NULL,
  `kode_record` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_tabungan`
--

CREATE TABLE `tb_tabungan` (
  `id_tabungan` int(12) NOT NULL,
  `id_nasabah` int(12) NOT NULL,
  `total_tabungan` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_tabungan`
--

INSERT INTO `tb_tabungan` (`id_tabungan`, `id_nasabah`, `total_tabungan`) VALUES
(10001231, 12310001, 0),
(10001232, 12310002, 0),
(10001233, 12310003, 0),
(10001234, 12310004, 0),
(10001235, 12310005, 0);

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `tb_fcm`
--
ALTER TABLE `tb_fcm`
  ADD PRIMARY KEY (`id_fcm`),
  ADD KEY `id_nasabah` (`id_nasabah`);

--
-- Indeks untuk tabel `tb_nasabah`
--
ALTER TABLE `tb_nasabah`
  ADD PRIMARY KEY (`id_nasabah`),
  ADD KEY `id_login` (`id_login`);

--
-- Indeks untuk tabel `tb_picture`
--
ALTER TABLE `tb_picture`
  ADD PRIMARY KEY (`id_picture`),
  ADD KEY `id_nasabah` (`id_nasabah`);

--
-- Indeks untuk tabel `tb_record`
--
ALTER TABLE `tb_record`
  ADD PRIMARY KEY (`id_record`),
  ADD KEY `id_nasabah` (`id_nasabah`);

--
-- Indeks untuk tabel `tb_tabungan`
--
ALTER TABLE `tb_tabungan`
  ADD PRIMARY KEY (`id_tabungan`),
  ADD KEY `id_nasabah` (`id_nasabah`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `tb_fcm`
--
ALTER TABLE `tb_fcm`
  MODIFY `id_fcm` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `tb_picture`
--
ALTER TABLE `tb_picture`
  MODIFY `id_picture` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33100005;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `tb_fcm`
--
ALTER TABLE `tb_fcm`
  ADD CONSTRAINT `tb_fcm_ibfk_1` FOREIGN KEY (`id_nasabah`) REFERENCES `tb_nasabah` (`id_nasabah`);

--
-- Ketidakleluasaan untuk tabel `tb_picture`
--
ALTER TABLE `tb_picture`
  ADD CONSTRAINT `tb_picture_ibfk_1` FOREIGN KEY (`id_nasabah`) REFERENCES `tb_nasabah` (`id_nasabah`);

--
-- Ketidakleluasaan untuk tabel `tb_record`
--
ALTER TABLE `tb_record`
  ADD CONSTRAINT `tb_record_ibfk_1` FOREIGN KEY (`id_nasabah`) REFERENCES `tb_nasabah` (`id_nasabah`);

--
-- Ketidakleluasaan untuk tabel `tb_tabungan`
--
ALTER TABLE `tb_tabungan`
  ADD CONSTRAINT `tb_tabungan_ibfk_1` FOREIGN KEY (`id_nasabah`) REFERENCES `tb_nasabah` (`id_nasabah`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
