package id.rival.banksampahcipondoh.config;

import android.content.Context;
import android.content.SharedPreferences;

import static id.rival.banksampahcipondoh.config.General.ALAMATNASABAH;
import static id.rival.banksampahcipondoh.config.General.IDLOGIN;
import static id.rival.banksampahcipondoh.config.General.IDNASABAH;
import static id.rival.banksampahcipondoh.config.General.NAMANASABAH;
import static id.rival.banksampahcipondoh.config.General.PASSNASABAH;
import static id.rival.banksampahcipondoh.config.General.TABUNGAN;
import static id.rival.banksampahcipondoh.config.General.TELPNASABAH;
import static id.rival.banksampahcipondoh.config.General.USER_IMAGE;

public class SessionManager {
    public static final String TAG = SessionManager.class.getSimpleName();
    private static final String IS_LOGIN = "isLogin";

    private SharedPreferences mPref;
    private SharedPreferences.Editor mEditor;
    private Context mContext;

    private static SessionManager instance;

    public SessionManager(Context pContext){
        this.mContext = pContext;
        mPref = this.mContext.getSharedPreferences(General.PREF_NAME, Context.MODE_PRIVATE);
        mEditor = mPref.edit();
    }

    public void saveAutoStart(boolean isOn) {
        mEditor.putBoolean(General.AUTO_START_LIST, isOn);
        mEditor.commit();
    }

    public boolean isAutoStartOn() {
        return mPref.getBoolean(General.AUTO_START_LIST, false);
    }

    public void logoutUser() {
        // Clearing all data from Shared Preferences
        SharedPreferences pref = mContext.getSharedPreferences(General.PREF_NAME, 0);
        SharedPreferences.Editor editor = pref.edit();
        editor.clear();
        editor.commit();
    }

    public String getStringFromSP(String pKey){
        return mPref.getString(pKey,"");
    }

    public void setStringToSP (String pKey, String pValue){
        mEditor.putString(pKey, pValue);
        mEditor.commit();
    }

    public void setIntToSP(String pKey, int pValue){
        mEditor.putInt(pKey, pValue);
        mEditor.commit();
    }

    public static SessionManager getInstance(Context context) {
        if (instance == null) {
            instance = new SessionManager(context);
        }
        return instance;
    }

    public void createLoginSession(String paramIdNasabah, String paramPassNasabah, String paramNamaNasabah,
                                   String paramIdLogin, String paramAlamatNasabah, String paramTelpNasabah,
                                   String paramTotalTabungan) {
        // Storing login value as TRUE
        mEditor.putBoolean(IS_LOGIN, true);
        mEditor.putString(IDNASABAH, paramIdNasabah);
        mEditor.putString(PASSNASABAH, paramPassNasabah);
        mEditor.putString(NAMANASABAH, paramNamaNasabah);
        mEditor.putString(ALAMATNASABAH, paramAlamatNasabah);
        mEditor.putString(TELPNASABAH, paramTelpNasabah);
        mEditor.putString(IDLOGIN, paramIdLogin);
        mEditor.putString(TABUNGAN, paramTotalTabungan);
        mEditor.commit();
    }

    public void imageProfile(String paramProfile){
        mEditor.putString(USER_IMAGE, paramProfile);
    }

    public int getIntFromSP (String pKey){
        return mPref.getInt(pKey, 0);
    }

    public void setBooleanToSP(String pKey, boolean pValue){
        mEditor.putBoolean(pKey, pValue);
        mEditor.commit();
    }

    public boolean getBooleanFromSP(String pKey){
        return mPref.getBoolean(pKey, false);
    }

    public boolean isLoggedIn(){
        return mPref.getBoolean(IS_LOGIN, false);
    }


}
