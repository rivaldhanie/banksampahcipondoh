package id.rival.banksampahcipondoh.config;

public class General {
    //region Status
    public static final int API_REQUEST_SUCCESS  = 200;
    public static final int API_REQUEST_FAIL = 202;
    //endregion

    public static final int SLEEP_TIME = 3000;

    //region Realm
    public static final int SCHEMA_VERSION = 1;
    public static final String SCHEMA_NAME = "banksampah.realm";
    //endregion

    //region Shared Preference
    public static final String PREF_NAME = "banksampahCipondoh";
    public static final int PRIVATE_MODE = 0;

    //Region Login Type
    public static final String ADMIN = "Admin";
    public static final String NASABAH = "Nasabah";
    //endregion

    public static final String IS_LOGIN = "islogin";
    public static final String IDNASABAH = "id_nasabah";
    public static final String PASSNASABAH = "pass_nasabah";
    public static final String NAMANASABAH = "nama_nasabah";
    public static final String ALAMATNASABAH = "alamat_nasabah";
    public static final String TELPNASABAH = "telp_nasabah";
    public static final String IDLOGIN = "id_login";
    public static final String USER_IMAGE = "userimage";
    public static final String FULLNAME = "fullname";
    public static final String EMAIL = "email";
    public static final String HP = "hp";
    public static final String LOGIN_TYPE = "loginType";
    public static final String LOGIN_TYPE_NAME = "loginTypeName";
    public static final String ADDRESS = "address";
    public static final String PHONE = "phone";
    public static final String IMAGE = "image";
    public static final String TABUNGAN = "total_tabungan";
    //endregion

    //region Key untuk permision result
    public static final int READ_PHONE_STATE_CONSTAT = 0;
    //endregion

    //region key simpleDateFormat
    public static final String YEAR_MILISECOND = "yyyyMMddHHmmssSSSS";
    public static final String GETDATE = "dd/MM/yyyy";
    //endregion

    public static final int flogSyncSuccess = 1;
    public static final int flogSyncFailed = 2;
    public static final String AUTO_START_LIST = "autoStartList";

    public static final String serverKey = "key=AAAATioPAqo:APA91bGqQefo3Mo905HuaPZwVDDx-x1kvDYeK_V6Ez52jB5FtHQrF6XjCmyXQWE9AIYMk4GUQP8Z7FqsBEahhaBpXb4C0fYz8luv3Ws2eD_FJdfMs81SerLcB7vJiU5U2rXZGzY-THBO";
}
