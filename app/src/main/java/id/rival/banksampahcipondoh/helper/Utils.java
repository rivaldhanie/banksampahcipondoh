package id.rival.banksampahcipondoh.helper;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import androidx.core.app.ActivityCompat;

import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import static id.rival.banksampahcipondoh.config.General.READ_PHONE_STATE_CONSTAT;

public class Utils {
    public static String modelToJson(Object param) {
        Gson gson = new Gson();
        String jsonIn = gson.toJson(param);
        Utils.showLog("utils", jsonIn);
        return jsonIn;
    }

    /**
     * conversi dari dp to pixel
     *
     * @param context Context App
     * @param dp      parameter dalam bentuk DP
     * @return Integer Pixel
     */
    public static int dpToPx(Context context, int dp) {
        Resources r = context.getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }

    //Conversi String currency ke double
    public static double convertCurrencyStringToDouble(String strCurrency) {
        String number = strCurrency.replaceAll("\\D+", "");
        return Double.parseDouble(number);
    }

    /**
     * format harga
     *
     * @param currency
     * @return
     */
    public static String currencyFormat(String currency) {
        if (currency == null) {
            currency = "0";
        }
        double currPrice = Double.valueOf(currency);
        DecimalFormat format = new DecimalFormat("#,###");
        DecimalFormatSymbols symbols = new DecimalFormatSymbols();
        symbols.setDecimalSeparator('.');
        symbols.setGroupingSeparator(',');
        format.setDecimalFormatSymbols(symbols);

        return format.format(currPrice);
    }

    /*hideSoftKeyboard*/
    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(
                Activity.INPUT_METHOD_SERVICE);
//        inputMethodManager.hideSoftInputFromWindow(
//                activity.getCurrentFocus().getWindowToken(), 0);
        View focusedView = activity.getCurrentFocus();
        if (focusedView != null) {
            inputMethodManager.hideSoftInputFromWindow(focusedView.getWindowToken(),
                    InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    public static String currencyFormatRp(String currency) {
        if (currency == null) {
            currency = "0";
        }
        double currPrice = Double.valueOf(currency);
        DecimalFormat format = new DecimalFormat("Rp #,###");
        DecimalFormatSymbols symbols = new DecimalFormatSymbols();
        symbols.setDecimalSeparator('.');
        symbols.setGroupingSeparator(',');
        format.setDecimalFormatSymbols(symbols);

        return format.format(currPrice);
    }

    public static String currencyFormatDecimal(String currency) {
        if (currency == null) {
            currency = "0";
        }
        double currPrice = Double.valueOf(currency);
        DecimalFormat format = new DecimalFormat("#,###.00");
        DecimalFormatSymbols symbols = new DecimalFormatSymbols();
        symbols.setDecimalSeparator('.');
        symbols.setGroupingSeparator(',');
        format.setDecimalFormatSymbols(symbols);

        return format.format(currPrice);
    }

    public static String currencyDoubleDecimal(String currency) {
        if (currency == null) {
            currency = "0";
        }
        double currPrice = Double.valueOf(currency);
        DecimalFormat format = new DecimalFormat("###0.00");
        DecimalFormatSymbols symbols = new DecimalFormatSymbols();
        symbols.setDecimalSeparator('.');
        symbols.setGroupingSeparator(',');
        format.setDecimalFormatSymbols(symbols);

        return format.format(currPrice);
    }

    public static String currencyDiscountDecimal(String currency){
        if (currency == null) {
            currency = "0";
        }
        double currPrice = Double.valueOf(currency);
        DecimalFormat format = new DecimalFormat("(#,###.00)");
        DecimalFormatSymbols symbols = new DecimalFormatSymbols();
        symbols.setDecimalSeparator('.');
        symbols.setGroupingSeparator(',');
        format.setDecimalFormatSymbols(symbols);

        return format.format(currPrice);
    }

    public static String formatRupiah(double number){
        Locale locale = new Locale("in", "ID");
        NumberFormat formatRupiah = NumberFormat.getCurrencyInstance(locale);
        return formatRupiah.format(number);
    }

    public static String convertDoubleToDecimal(double pDblValue) {
        DecimalFormat format = new DecimalFormat("###0.00");
        DecimalFormatSymbols symbols = new DecimalFormatSymbols();
        symbols.setDecimalSeparator('.');
        format.setDecimalFormatSymbols(symbols);

        return format.format(pDblValue);
    }

    public static void showSnackbar(View view, String pesan) {
        Snackbar.make(view, pesan, Snackbar.LENGTH_LONG)
                .show();
    }

    public static void showLog(String tag, String log) {
        //if (BuildConfig.DEBUG) {
        if (log == null) {
            log = "";
        }

        short maxLogSize = 1000;
        if (log.length() >= maxLogSize) {
            for (int i = 0; i <= log.length() / maxLogSize; ++i) {
                int start = i * maxLogSize;
                int end = (i + 1) * maxLogSize;
                end = end > log.length() ? log.length() : end;
                Log.i(tag, log.substring(start, end));
            }
        } else {
            Log.i(tag, log);
        }
        //}

    }

    public static String convertFromDoubleToString(double pDblValue) {
        DecimalFormat decimalFormat = new DecimalFormat("#");
        //return String.valueOf(pDblValue).replace(".0", "");
        return decimalFormat.format(pDblValue);
    }

    public static double convertFromStringToDouble(String pStringValue) {
        return Double.parseDouble(pStringValue);
    }

    public static String formatSmartCard(String pStrCardRef, String pStrLocRef,
                                         String pStrSaldoRefund, String pStrSaldoNonRefund) {

        Utils.showLog("smartCard", "write to smartcard "
                + "01"
                + "-" + pStrCardRef
                + "-" + pStrLocRef
                + "-" + pStrSaldoRefund
                + "-" + pStrSaldoNonRefund
                + "-" + "1"); // status top up --> sudah pernah topup


        return "01"
                + "-" + pStrCardRef
                + "-" + pStrLocRef
                + "-" + pStrSaldoRefund
                + "-" + pStrSaldoNonRefund
                + "-" + "1";
    }

    public static String formatSmartCardRegister(String pStrCardRef, String pStrLocRef) {

        Utils.showLog("smartCard", "write first to smartcard "
                + "01"
                + "-" + pStrCardRef
                + "-" + pStrLocRef
                + "-" + "0"
                + "-" + "0"
                + "-" + "0"); // status top up --> sudah pernah topup

        return "01"
                + "-" + pStrCardRef
                + "-" + pStrLocRef
                + "-" + "0" // saldo refunable
                + "-" + "0" // saldo non refunable
                + "-" + "0"; // state top up
    }

    /**
     * @param simpleDateFormat
     * @return
     */
    public static String formatDate(String simpleDateFormat) {
        String formattedDate;

        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat(simpleDateFormat);
        formattedDate = df.format(c.getTime());
        System.out.println("Current time =&gt; " + formattedDate);

        return formattedDate;
    }

    public static String cardRefFormat(String pStrLocRef, String pStrUserRef, String pStrTimeFormat) {

        Utils.showLog("cardRef", "cardRef: "
                + pStrLocRef
                + "#" + pStrUserRef
                + "#" + pStrTimeFormat);

        return pStrLocRef
                + "#" + pStrUserRef
                + "#" + pStrTimeFormat;
    }

    /**
     * method untuk check read phone state
     *
     * @param c Context
     * @return boolean
     */
    public static boolean checkReadPhoneState(Context c) {
        String permission = Manifest.permission.READ_PHONE_STATE;
        int res = c.checkCallingOrSelfPermission(permission);
        return (res == PackageManager.PERMISSION_GRANTED);
    }

    /**
     * menampilkan dialog permission untuk read telpon
     *
     * @param activity
     */
    public static void showDialogPermmisionReadPhoneState(Activity activity) {
        ActivityCompat.requestPermissions(activity, new String[]{
                Manifest.permission.READ_PHONE_STATE}, READ_PHONE_STATE_CONSTAT);
    }


    /**
     * konversi dari hex string ke dalam string biasa
     *
     * @param hex -> HexDecimal dalam bentuk String, ex: 31323334
     * @return String. ex: 1234
     */
    public static String convertHexToString(String hex) {

        StringBuilder sb = new StringBuilder();
        StringBuilder temp = new StringBuilder();

        //49204c6f7665204a617661 split into two characters 49, 20, 4c...
        for (int i = 0; i < hex.length() - 1; i += 2) {

            //grab the hex in pairs
            String output = hex.substring(i, (i + 2));
            //convert hex to decimal
            int decimal = Integer.parseInt(output, 16);
            //convert the decimal to character
            sb.append((char) decimal);

            temp.append(decimal);
        }
        System.out.println("Decimal : " + temp.toString());

        return sb.toString();
    }

    private final static char[] hexArray = "0123456789ABCDEF".toCharArray();

    /**
     * conversi dari bytes ke dalam hex decimal, namun resultnya dalam bentuk string
     *
     * @param bytes bytes from card
     * @return String. ex: 31323334
     */
    public static String convertBytesToHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        for (int j = 0; j < bytes.length; j++) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }

    /**
     * convert langsung dari bytes ke dalam string final.
     *
     * @param bytes bytes from card.
     * @return String
     */
    public static String convertBytesToString(byte[] bytes) {
        String strFromConvert = convertHexToString(convertBytesToHex(bytes));
        char[] charConvertString = strFromConvert.toCharArray();
        char[] charFinalResult = new char[0];
        int startFrom = 0;
        String strFinalResult = "";
        for (int i = 0; i < charConvertString.length; i++) {
            if (!String.valueOf(charConvertString[i]).equals("-")) {
                Log.i("convert", "position " + i + " " + charConvertString[i] + " " + startFrom);
                charFinalResult = new char[startFrom + 1];
                charFinalResult[startFrom] = charConvertString[i];
                startFrom = startFrom + 1;
                strFinalResult += String.valueOf(charConvertString[i]);

            }
        }

        return strFinalResult;
    }

}
