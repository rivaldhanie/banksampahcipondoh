package id.rival.banksampahcipondoh.helper;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.view.View;
import id.rival.banksampahcipondoh.R;

import com.google.android.material.snackbar.Snackbar;

public class UtilsSnackbar {
    public static void showSnackbarTypeOne(View rootView, String mMessage){
        Snackbar.make(rootView, mMessage, Snackbar.LENGTH_LONG)
                .setAction("Action", null)
                .show();
    }

    public static void showSnackbarTypeTwo(View rootView, String mMessage){
        Snackbar.make(rootView, mMessage, Snackbar.LENGTH_LONG)
                .make(rootView, mMessage, Snackbar.LENGTH_INDEFINITE)
                .setAction("Action", null)
                .show();
    }

    public static void showSnakbarTypeThree(View rootView, final Activity activity) {
        Snackbar.make(rootView, activity.getString(R.string.no_internet_connectivity), Snackbar.LENGTH_INDEFINITE)
                .setAction(activity.getString(R.string.try_again), new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = activity.getIntent();
                        activity.finish();
                        activity.startActivity(intent);
                    }
                })
                .setActionTextColor(Color.CYAN)
                .setCallback(new Snackbar.Callback() {
                    @Override
                    public void onDismissed(Snackbar snackbar, int event) {
                        super.onDismissed(snackbar, event);
                    }

                    @Override
                    public void onShown(Snackbar snackbar) {
                        super.onShown(snackbar);
                    }
                })
                .show();

    }

    public static void showSnakbarTypeFour(View rootView, final Activity activity, String mMessage) {
        Snackbar.make(rootView, mMessage, Snackbar.LENGTH_INDEFINITE)
                .setAction(activity.getString(R.string.try_again), new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = activity.getIntent();
                        activity.finish();
                        activity.startActivity(intent);
                    }
                })
                .setActionTextColor(Color.CYAN)
                .setCallback(new Snackbar.Callback() {
                    @Override
                    public void onDismissed(Snackbar snackbar, int event) {
                        super.onDismissed(snackbar, event);
                    }

                    @Override
                    public void onShown(Snackbar snackbar) {
                        super.onShown(snackbar);
                    }
                })
                .show();

    }
}
