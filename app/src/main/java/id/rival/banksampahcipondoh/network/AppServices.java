package id.rival.banksampahcipondoh.network;



import java.util.List;

import id.rival.banksampahcipondoh.model.Response;
import id.rival.banksampahcipondoh.view.cropper.model.PictureResponse;
import id.rival.banksampahcipondoh.view.login.model.LoginResponse;
import id.rival.banksampahcipondoh.view.main_menu.model.TabunganResponse;
import id.rival.banksampahcipondoh.view.member.model.MemberHistoryModel;
import id.rival.banksampahcipondoh.view.member.model.MemberListResponse;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface AppServices {

    @FormUrlEncoded
    @POST("login.php")
    Call<LoginResponse> PostLoginUserSVC(@Field("id_nasabah") String userId,
                                            @Field("pass_nasabah") String password);

    @FormUrlEncoded
    @POST("getTabungan.php")
    Call<TabunganResponse> GetTabunganData(@Field("userId") String userId);

    @FormUrlEncoded
    @POST("updateProfile.php")
    Call<Response> updateProfile(@Field("JSON_IN") String json_in);

    @FormUrlEncoded
    @POST("getNasabah.php")
    Call<MemberListResponse> getNasabah(@Field("JSON_IN") String json_in);

    @FormUrlEncoded
    @POST("selectMemberHistory.php")
    Call<List<MemberHistoryModel>> getHistory(@Field("id_nasabah") String id_nasabah);

    @FormUrlEncoded
    @POST("saveImage.php")
    Call<PictureResponse>postPicture(@Field("path_picture") String path_picture,
                                     @Field("id_nasabah") String id_nasabah,
                                     @Field("ket_picture") String ket_picture);

    @FormUrlEncoded
    @POST("getImage.php")
    Call<PictureResponse>getPicture(@Field("id_nasabah") String id_nasabah,
                                    @Field("ket_picture") String ket_picture);

    @FormUrlEncoded
    @POST("setFcm.php")
    Call<Response> setFCM(@Field("id_nasabah") String id_nasabah,
                                    @Field("fcm") String fcm);

    @FormUrlEncoded
    @POST("saveTransaksi.php")
    Call<Response>saveTransaksi(@Field("JSON_IN") String jsonin);
}
