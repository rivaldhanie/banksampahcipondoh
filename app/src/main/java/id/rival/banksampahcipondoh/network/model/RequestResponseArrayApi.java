package id.rival.banksampahcipondoh.network.model;

import java.util.List;

public class RequestResponseArrayApi<T extends Object> {
    private int status;
    private String message;
    private List<T> data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<T> getData() {
        return data;
    }

    public void setData(List<T> data) {
        this.data = data;
    }
}
