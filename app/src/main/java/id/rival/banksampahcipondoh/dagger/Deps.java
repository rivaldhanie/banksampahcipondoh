package id.rival.banksampahcipondoh.dagger;

import javax.inject.Singleton;

import dagger.Component;
import id.rival.banksampahcipondoh.view.cropper.process.CropPicturePresenter;
import id.rival.banksampahcipondoh.view.login.process.LoginPresenter;
import id.rival.banksampahcipondoh.view.main_menu.process.MainMenuPresenter;
import id.rival.banksampahcipondoh.view.member.process.MemberDetailPresenter;
import id.rival.banksampahcipondoh.view.member.process.MemberListPresenter;
import id.rival.banksampahcipondoh.view.profil.process.MenuProfilPresenter;
import id.rival.banksampahcipondoh.view.transaksi.process.PenarikanPresenter;

@Singleton
@Component(modules = {
        ApplicationModule.class,
        NetworkModule.class,
        PresenterModule.class,
        SessionManagerModule.class
})

public interface Deps {

    void inject(LoginPresenter loginPresenter);
    void inject(MainMenuPresenter mainMenuPresenter);
    void inject(MenuProfilPresenter menuProfilPresenter);
    void inject(MemberListPresenter memberListPresenter);
    void inject(MemberDetailPresenter memberDetailPresenter);
    void inject(CropPicturePresenter cropPicturePresenter);
    void inject(PenarikanPresenter penarikanPresenter);
}
