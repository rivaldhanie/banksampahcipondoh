package id.rival.banksampahcipondoh.dagger;

import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import id.rival.banksampahcipondoh.view.login.process.LoginPresenter;

@Module
public class PresenterModule {

    @Provides
    @Singleton
    LoginPresenter loginPresenter(Context context){
        return new LoginPresenter(context);
    }
}
