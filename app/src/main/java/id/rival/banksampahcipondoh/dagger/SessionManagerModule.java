package id.rival.banksampahcipondoh.dagger;

import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import id.rival.banksampahcipondoh.config.SessionManager;

@Module
public class SessionManagerModule {
    @Provides
    @Singleton
    SessionManager providesSessionManager(Context context){
        return  new SessionManager(context);
    }
}
