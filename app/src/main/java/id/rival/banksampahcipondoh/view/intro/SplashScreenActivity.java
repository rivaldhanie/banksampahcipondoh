package id.rival.banksampahcipondoh.view.intro;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;


import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;

import java.util.HashMap;

import id.rival.banksampahcipondoh.BuildConfig;
import id.rival.banksampahcipondoh.R;
import id.rival.banksampahcipondoh.config.SessionManager;
import id.rival.banksampahcipondoh.view.login.ui.LoginActivity;
import id.rival.banksampahcipondoh.view.main_menu.view.MainMenuActivity;

public class SplashScreenActivity extends AppCompatActivity {
    SessionManager sessionManager;
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        sessionManager = new SessionManager(this);
        context = SplashScreenActivity.this;
        getAppVersion();
    }

    private void doLogin() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                checkSessionLogin();
            }
        }, 2000);
    }

    private void getAppVersion() {
        FirebaseRemoteConfig mFirebaseRemoteConfig = FirebaseRemoteConfig.getInstance();
        mFirebaseRemoteConfig.setConfigSettingsAsync(new FirebaseRemoteConfigSettings.Builder()
                .build());
        HashMap<String, Object> defaults = new HashMap<>();
        mFirebaseRemoteConfig.setDefaultsAsync(defaults);

        final Task<Void> fetch = mFirebaseRemoteConfig.fetch();
        fetch.addOnCompleteListener(this, new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()){
                    mFirebaseRemoteConfig.fetchAndActivate();
                    long versionCode = BuildConfig.VERSION_CODE;
                    if (versionCode < mFirebaseRemoteConfig.getLong("current_version")){
                        showDialogUpdate();
                    } else {
                        doLogin();
                    }
                }
            }
        });
    }

    private void showDialogUpdate() {
        AlertDialog dialog = new AlertDialog.Builder(this)
                .setTitle("Aplikasi versi terbaru sudah tersedia")
                .setMessage("Silahkan perbarui aplikasi anda.")
                .setPositiveButton("Perbarui",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("www.google.com"));
                                startActivity(intent);
                            }
                        }).setNegativeButton("Tidak, Terima kasih",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                doLogin();
                            }
                        }).create();
        dialog.setCancelable(false);
        dialog.show();
    }

    private void checkSessionLogin() {
        Class classActivity;
        if (sessionManager.isLoggedIn()){
            classActivity = MainMenuActivity.class;
        } else {
            classActivity = LoginActivity.class;
        }

        Intent intent = new Intent(context, classActivity);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
        finish();

    }

}