package id.rival.banksampahcipondoh.view.member.model;

public class MemberHistoryModel {
    String id_record;
    String id_nasabah;
    String ket_record;
    String jumlah_record;
    String tanggal_record;
    String kode_record;

    public String getId_record() {
        return id_record;
    }

    public void setId_record(String id_record) {
        this.id_record = id_record;
    }

    public String getId_nasabah() {
        return id_nasabah;
    }

    public void setId_nasabah(String id_nasabah) {
        this.id_nasabah = id_nasabah;
    }

    public String getKet_record() {
        return ket_record;
    }

    public void setKet_record(String ket_record) {
        this.ket_record = ket_record;
    }

    public String getJumlah_record() {
        return jumlah_record;
    }

    public void setJumlah_record(String jumlah_record) {
        this.jumlah_record = jumlah_record;
    }

    public String getTanggal_record() {
        return tanggal_record;
    }

    public void setTanggal_record(String tanggal_record) {
        this.tanggal_record = tanggal_record;
    }

    public String getKode_record() {
        return kode_record;
    }

    public void setKode_record(String kode_record) {
        this.kode_record = kode_record;
    }
}
