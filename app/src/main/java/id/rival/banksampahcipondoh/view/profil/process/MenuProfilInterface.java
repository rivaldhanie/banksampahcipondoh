package id.rival.banksampahcipondoh.view.profil.process;

import id.rival.banksampahcipondoh.base.BaseInterface;
import id.rival.banksampahcipondoh.view.profil.model.MenuProfilData;

public interface MenuProfilInterface extends BaseInterface {
    void onSuccessUpdateData(String message);
    void onFailedUpdateData(String message);
}
