package id.rival.banksampahcipondoh.view.member.process;

import java.util.List;

import id.rival.banksampahcipondoh.base.BaseInterface;
import id.rival.banksampahcipondoh.view.member.model.MemberHistoryModel;

public interface MemberDetailInterface extends BaseInterface {
    void onSuccessGetHistoryMember(List<MemberHistoryModel> dataHistory);
    void onSuccessNoData(String message);
    void onFailedGetHistoryMember(String message);
}
