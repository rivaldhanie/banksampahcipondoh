package id.rival.banksampahcipondoh.view.profil.process;

import android.content.Context;
import android.util.Log;

import javax.inject.Inject;

import id.rival.banksampahcipondoh.base.BaseApplication;
import id.rival.banksampahcipondoh.base.BasePresenter;
import id.rival.banksampahcipondoh.config.General;
import id.rival.banksampahcipondoh.config.SessionManager;
import id.rival.banksampahcipondoh.helper.Utils;
import id.rival.banksampahcipondoh.model.Response;
import id.rival.banksampahcipondoh.network.AppServices;
import id.rival.banksampahcipondoh.network.model.RequestResponse;
import id.rival.banksampahcipondoh.view.profil.model.MenuProfilParam;
import retrofit2.Call;
import retrofit2.Callback;

public class MenuProfilPresenter implements BasePresenter<MenuProfilInterface> {

    public static final String TAG = MenuProfilPresenter.class.getSimpleName();

    @Inject
    AppServices appServices;
    @Inject
    SessionManager mSessionManager;

    private Context context;
    private MenuProfilInterface view;

    public MenuProfilPresenter(Context context){
        super();
        this.context = context;
        this.mSessionManager = new SessionManager(context);
        ((BaseApplication) context.getApplicationContext()).getDeps().inject(this);

    }

    public void updateProfile (MenuProfilParam param){
        view.showLoader();
        Call<Response> call = appServices.updateProfile(Utils.modelToJson(param));
        call.enqueue(new Callback<Response>() {
            @Override
            public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
                view.dismissLoader();
                Response body = response.body();
                if (body.getStatus() == General.API_REQUEST_SUCCESS){
                    view.onSuccessUpdateData(body.getMessage());
                }else {
                    view.onFailedUpdateData(body.getMessage());
                }
            }

            @Override
            public void onFailure(Call<Response> call, Throwable t) {
                view.dismissLoader();
                view.onNetworkError("Jaringan Sedang Bermasalah");
            }
        });
    }



    @Override
    public void setupView(MenuProfilInterface view) {
        this.view = view;
    }

    @Override
    public void clearView() {
        view = null;
    }
}
