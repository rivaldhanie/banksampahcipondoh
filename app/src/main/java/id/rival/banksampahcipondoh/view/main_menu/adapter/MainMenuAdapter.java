package id.rival.banksampahcipondoh.view.main_menu.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import id.rival.banksampahcipondoh.R;
import id.rival.banksampahcipondoh.view.main_menu.model.MainMenuModel;
import id.rival.banksampahcipondoh.view.member.ui.MemberListActivity;
import id.rival.banksampahcipondoh.view.notifikasi.ui.NotifikasiActivity;
import id.rival.banksampahcipondoh.view.profil.ui.MenuProfilActivity;

public class MainMenuAdapter extends BaseAdapter {
    private Context context;
    private List<MainMenuModel> list;


    public static class ViewHolder{
        ImageView btImage;
        TextView txAppName;
        LinearLayout item;
    }

    public MainMenuAdapter (Context context, List<MainMenuModel> list){
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null){
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.main_menu_adapter, parent, false);

            holder.btImage = convertView.findViewById(R.id.img_menu);
            holder.txAppName = convertView.findViewById(R.id.txt_app_name);
            holder.item = convertView.findViewById(R.id.layout_menu_button);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        MainMenuModel model = list.get(position);
        holder.btImage.setImageResource(model.getMenuIcon());
        holder.txAppName.setText(model.getMenuName());


        holder.item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (model.getMenuName()){
                    case "Profil":
                        Intent intentProfil = new Intent(context, MenuProfilActivity.class);
                        context.startActivity(intentProfil);
                        break;
                    case "Member List" :
                        Intent intentMember = new Intent(context, MemberListActivity.class);
                        context.startActivity(intentMember);
                        break;
                    case "Tabungan" :
                        break;
                    case "Notifikasi" :
                        Intent intentNotifikasi = new Intent(context, NotifikasiActivity.class);
                        context.startActivity(intentNotifikasi);
                        break;
                }
            }
        });

        return convertView;
    }

}
