package id.rival.banksampahcipondoh.view.member.process;

import android.content.Context;

import javax.inject.Inject;

import id.rival.banksampahcipondoh.base.BaseApplication;
import id.rival.banksampahcipondoh.base.BasePresenter;
import id.rival.banksampahcipondoh.network.AppServices;
import id.rival.banksampahcipondoh.view.member.model.MemberListResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MemberListPresenter implements BasePresenter<MemberListInterface> {
    @Inject
    AppServices appServices;

    private MemberListInterface view;
    private Context context;

    public MemberListPresenter(Context context){
        super();
        this.context=context;
        ((BaseApplication)context.getApplicationContext()).getDeps().inject(this);
    }

    public void getAllNasabah(String jsonIn){
        view.showLoader();
        Call<MemberListResponse> call = appServices.getNasabah(jsonIn);
        call.enqueue(new Callback<MemberListResponse>() {
            @Override
            public void onResponse(Call<MemberListResponse> call, Response<MemberListResponse> response) {
                view.dismissLoader();
                if (response.isSuccessful()){
                    if (response.body().getStatus() == 200){
                        view.onSuccessGetMemberList(response.body().getDataNasabah());
                    } else {
                        view.onFailedGetMemberList("Tidak Ada Data");
                    }
                } else {
                    view.onNetworkError("Jaringan Error");
                }
            }

            @Override
            public void onFailure(Call<MemberListResponse> call, Throwable t) {
                view.dismissLoader();
                view.onNetworkError(String.valueOf(t));
            }
        });
    }

    @Override
    public void setupView(MemberListInterface view) {
        this.view = view;
    }

    @Override
    public void clearView() {
        view = null;
    }
}
