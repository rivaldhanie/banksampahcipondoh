package id.rival.banksampahcipondoh.view.member.process;

import android.content.Context;

import java.util.List;

import javax.inject.Inject;

import id.rival.banksampahcipondoh.base.BaseApplication;
import id.rival.banksampahcipondoh.base.BasePresenter;
import id.rival.banksampahcipondoh.network.AppServices;
import id.rival.banksampahcipondoh.view.member.model.MemberHistoryModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MemberDetailPresenter implements BasePresenter<MemberDetailInterface> {
    @Inject
    AppServices appServices;

    private MemberDetailInterface view;
    private Context context;

    public MemberDetailPresenter(Context context){
        super();
        this.context = context;
        ((BaseApplication)context.getApplicationContext()).getDeps().inject(this);
    }

    public void getHistory(String id_nasabah){
        view.showLoader();
        Call<List<MemberHistoryModel>> call = appServices.getHistory(id_nasabah);
        call.enqueue(new Callback<List<MemberHistoryModel>>() {
            @Override
            public void onResponse(Call<List<MemberHistoryModel>> call, Response<List<MemberHistoryModel>> response) {
                view.dismissLoader();
                if (response.isSuccessful()){
                    if (response.body().size() > 0){
                        view.onSuccessGetHistoryMember(response.body());
                    } else {
                        view.onSuccessNoData("no data");
                    }
                } else {
                    view.onNetworkError("Jaringan Error");
                }
            }

            @Override
            public void onFailure(Call<List<MemberHistoryModel>> call, Throwable t) {
                view.dismissLoader();
                view.onNetworkError(String.valueOf(t));
            }
        });
    }

    @Override
    public void setupView(MemberDetailInterface view) {
        this.view = view;
    }

    @Override
    public void clearView() {
        view = null;
    }
}
