package id.rival.banksampahcipondoh.view.member.ui;

import androidx.appcompat.widget.Toolbar;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.rival.banksampahcipondoh.R;
import id.rival.banksampahcipondoh.base.BaseActivity;
import id.rival.banksampahcipondoh.config.SessionManager;
import id.rival.banksampahcipondoh.view.member.adapter.MemberListAdapter;
import id.rival.banksampahcipondoh.view.member.model.MemberListModel;
import id.rival.banksampahcipondoh.view.member.process.MemberListInterface;
import id.rival.banksampahcipondoh.view.member.process.MemberListPresenter;

public class MemberListActivity extends BaseActivity implements MemberListInterface, View.OnClickListener {

    private List<MemberListModel> list = new ArrayList<>();
    private ArrayList arrayList =new ArrayList<>();
    SessionManager sessionManager;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.list_nasabah)
    ListView listMember;
    @BindView(R.id.edt_search)
    EditText search;
    @BindView(R.id.btsearch)
    ImageButton btSearch;
    MemberListPresenter memberListPresenter;
    MemberListAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_member_list);
        ButterKnife.bind(this);

        memberListPresenter = new MemberListPresenter(this);
        memberListPresenter.setupView(this);
        sessionManager = new SessionManager(getApplicationContext());

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Member List");

        memberListPresenter.getAllNasabah("");

        btSearch.setOnClickListener(this);

        search.addTextChangedListener(textWatcher);

    }

    private TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            arrayList.clear();
            for (int i = 0; i < list.size(); i++){
                String id = list.get(i).getId_nasabah();
                String nama = list.get(i).getNama_nasabah();
                if (id.contains(s)){
                    arrayList.addAll(Collections.singleton(list.get(i)));
                }
                if (list.get(i).getNama_nasabah().toLowerCase().contains(s) || list.get(i).getNama_nasabah().toUpperCase().contains(s)){
                    arrayList.addAll(Collections.singleton(list.get(i)));
                }
            }
        }

        @Override
        public void afterTextChanged(Editable s) {
            listMember.setVisibility(View.GONE);
            listMember.setAdapter(null);
            adapter = new MemberListAdapter(getApplicationContext(), arrayList);
            listMember.setAdapter(adapter);
            adapter.notifyDataSetChanged();
            listMember.setVisibility(View.VISIBLE);
        }
    };

    @Override
    public void showLoader() {
        viewLoader();
    }

    @Override
    public void dismissLoader() {
        hiddenLoader();
    }

    @Override
    public void onNetworkError(String appErrorMessage) {
        Toast.makeText(this, appErrorMessage, Toast.LENGTH_SHORT).show();
    }


    @Override
    public void onSuccessGetMemberList(List<MemberListModel> dataNasabah) {
        listMember.setVisibility(View.GONE);
        listMember.setAdapter(null);
        listMember.setVisibility(View.VISIBLE);
        list.addAll(dataNasabah);
        adapter = new MemberListAdapter(this, list);
        listMember.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onFailedGetMemberList(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btsearch:
                listMember.setVisibility(View.GONE);
                String param = search.getText().toString();
                for (int i = 0; i < list.size(); i++){
                    String nama = list.get(i).getNama_nasabah();
                    String id = list.get(i).getId_nasabah();
                    if (nama.contains(param) || id.equals(param)){
                        list.addAll(list);
                    } else {
                        list.remove(list);
                    }
                }
                listMember.setVisibility(View.VISIBLE);
                adapter = new MemberListAdapter(this, list);
                listMember.setAdapter(adapter);
                adapter.notifyDataSetChanged();
        }
    }
}