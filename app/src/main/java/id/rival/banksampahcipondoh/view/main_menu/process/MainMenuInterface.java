package id.rival.banksampahcipondoh.view.main_menu.process;

import id.rival.banksampahcipondoh.base.BaseInterface;
import id.rival.banksampahcipondoh.view.cropper.model.PictureResponse;
import id.rival.banksampahcipondoh.view.main_menu.model.DataTabungan;

public interface MainMenuInterface extends BaseInterface {

//    void onSuccessGetTabungan(DataTabungan dataTabungan);
    void onSuccessGetImage(PictureResponse pictureResponse);
    void onSuccessSetFcm(String message);
    void onFailed(String message);


}
