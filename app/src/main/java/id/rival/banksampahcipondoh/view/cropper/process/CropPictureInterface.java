package id.rival.banksampahcipondoh.view.cropper.process;

import id.rival.banksampahcipondoh.base.BaseInterface;
import id.rival.banksampahcipondoh.view.cropper.model.PictureResponse;

public interface CropPictureInterface extends BaseInterface {
    void onSuccessUploadPicture(PictureResponse pictureResponse);
    void onFailedUploadPicture(String message);
}
