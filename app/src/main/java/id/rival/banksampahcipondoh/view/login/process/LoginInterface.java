package id.rival.banksampahcipondoh.view.login.process;

import id.rival.banksampahcipondoh.base.BaseInterface;
import id.rival.banksampahcipondoh.view.login.model.LoginData;

public interface LoginInterface extends BaseInterface {
    void loginSuccess(LoginData loginData);
    void loginFailed(String paramStrErrorMessage);

}
