package id.rival.banksampahcipondoh.view.login.ui;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.rival.banksampahcipondoh.R;
import id.rival.banksampahcipondoh.config.SessionManager;
import id.rival.banksampahcipondoh.helper.Utils;
import id.rival.banksampahcipondoh.helper.UtilsSnackbar;
import id.rival.banksampahcipondoh.view.login.model.LoginData;
import id.rival.banksampahcipondoh.view.login.process.LoginInterface;
import id.rival.banksampahcipondoh.view.login.process.LoginPresenter;
import id.rival.banksampahcipondoh.view.main_menu.view.MainMenuActivity;

public class LoginActivity extends AppCompatActivity implements LoginInterface {

    @BindView(R.id.loginUserId)
    EditText userIdTxt;
    @BindView(R.id.loginPassword)
    EditText passUserTxt;
    @BindView(R.id.btn_login)
    Button btnLogin;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.layout)
    RelativeLayout layout;

    private static final String TAG = LoginActivity.class.getSimpleName();

    LoginPresenter loginPresenter;
    private SessionManager sessionManager;
    String idNasabah, passNasabah;
    String username, alamat, tanggal_lahir, no_telp, loginStatus, totalTabungan;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        loginPresenter = new LoginPresenter(this);
        loginPresenter.setupView(this);
        sessionManager = new SessionManager(getApplicationContext());
    }


    @OnClick(R.id.btn_login)
    public void btnLoginClick(){
        Utils.hideSoftKeyboard(this);
        idNasabah = userIdTxt.getText().toString().trim();
        passNasabah = passUserTxt.getText().toString().trim();
        if (validate()){
            loginPresenter.PostLoginUserSVC(idNasabah, passNasabah);
        }
    }

    private boolean validate(){
        boolean valid = true;
        if (idNasabah.equals("")){
            userIdTxt.setError("Harus Diisi");
            valid = false;
        } else if (passNasabah.equals("")){
            passUserTxt.setError("Harus Diisi");
            valid = false;
        }
        return valid;
    }

    @Override
    public void loginSuccess(LoginData data) {
        idNasabah = data.getId_nasabah();
        Log.d(TAG, idNasabah);
        username = data.getNama_nasabah();
        alamat = data.getAlamat_nasabah();
        no_telp = data.getTelp_nasabah();
        passNasabah = data.getPass_nasabah();
        loginStatus = data.getId_login();
        totalTabungan = data.getTotal_tabungan();

        sessionManager.createLoginSession(idNasabah, passNasabah, username, loginStatus ,alamat, no_telp, totalTabungan);

        if (loginStatus.equals("101")){
            Intent intent = new Intent(this, MainMenuActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            finish();
        } else {
            Intent intent = new Intent(this, MainMenuActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            finish();
        }


    }

    @Override
    public void loginFailed(String paramStrErrorMessage) {
        dismissLoader();
        Utils.hideSoftKeyboard(LoginActivity.this);
        Toast.makeText(this, "Gagal", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showLoader() {
        btnLogin.setText("");
        btnLogin.setEnabled(false);
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void dismissLoader() {
        btnLogin.setText("masuk");
        btnLogin.setEnabled(true);
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onNetworkError(String appErrorMessage) {
        Utils.hideSoftKeyboard(LoginActivity.this);
        UtilsSnackbar.showSnakbarTypeThree(layout, this);
    }
}