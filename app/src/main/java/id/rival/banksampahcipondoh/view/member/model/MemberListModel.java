package id.rival.banksampahcipondoh.view.member.model;

public class MemberListModel {
    String id_nasabah;
    String pass_nasabah;
    String nama_nasabah;
    String alamat_nasabah;
    String telp_nasabah;
    String id_login;
    String id_tabungan;
    String total_tabungan;

    public String getId_nasabah() {
        return id_nasabah;
    }

    public void setId_nasabah(String id_nasabah) {
        this.id_nasabah = id_nasabah;
    }

    public String getPass_nasabah() {
        return pass_nasabah;
    }

    public void setPass_nasabah(String pass_nasabah) {
        this.pass_nasabah = pass_nasabah;
    }

    public String getNama_nasabah() {
        return nama_nasabah;
    }

    public void setNama_nasabah(String nama_nasabah) {
        this.nama_nasabah = nama_nasabah;
    }

    public String getAlamat_nasabah() {
        return alamat_nasabah;
    }

    public void setAlamat_nasabah(String alamat_nasabah) {
        this.alamat_nasabah = alamat_nasabah;
    }

    public String getTelp_nasabah() {
        return telp_nasabah;
    }

    public void setTelp_nasabah(String telp_nasabah) {
        this.telp_nasabah = telp_nasabah;
    }

    public String getId_login() {
        return id_login;
    }

    public void setId_login(String id_login) {
        this.id_login = id_login;
    }

    public String getTotal_tabungan() {
        return total_tabungan;
    }

    public void setTotal_tabungan(String total_tabungan) {
        this.total_tabungan = total_tabungan;
    }

    public String getId_tabungan() {
        return id_tabungan;
    }

    public void setId_tabungan(String id_tabungan) {
        this.id_tabungan = id_tabungan;
    }

}
