package id.rival.banksampahcipondoh.view.login.model;

public class LoginData {
    private String id_nasabah;
    private String nama_nasabah;
    private String pass_nasabah;
    private String alamat_nasabah;
    private String telp_nasabah;
    private String id_login;
    private String total_tabungan;

    public String getId_nasabah() {
        return id_nasabah;
    }

    public void setId_nasabah(String id_nasabah) {
        this.id_nasabah = id_nasabah;
    }

    public String getNama_nasabah() {
        return nama_nasabah;
    }

    public void setNama_nasabah(String nama_nasabah) {
        this.nama_nasabah = nama_nasabah;
    }

    public String getPass_nasabah() {
        return pass_nasabah;
    }

    public void setPass_nasabah(String pass_nasabah) {
        this.pass_nasabah = pass_nasabah;
    }

    public String getAlamat_nasabah() {
        return alamat_nasabah;
    }

    public void setAlamat_nasabah(String alamat_nasabah) {
        this.alamat_nasabah = alamat_nasabah;
    }

    public String getTelp_nasabah() {
        return telp_nasabah;
    }

    public void setTelp_nasabah(String telp_nasabah) {
        this.telp_nasabah = telp_nasabah;
    }

    public String getId_login() {
        return id_login;
    }

    public void setId_login(String id_login) {
        this.id_login = id_login;
    }

    public String getTotal_tabungan() {
        return total_tabungan;
    }

    public void setTotal_tabungan(String total_tabungan) {
        this.total_tabungan = total_tabungan;
    }
}
