package id.rival.banksampahcipondoh.view.main_menu.model;

public class TabunganParam {
    String userId;
    String tabungan;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getTabungan() {
        return tabungan;
    }

    public void setTabungan(String tabungan) {
        this.tabungan = tabungan;
    }
}
