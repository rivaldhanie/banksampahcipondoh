package id.rival.banksampahcipondoh.view.transaksi.process;

import android.content.Context;

import javax.inject.Inject;

import id.rival.banksampahcipondoh.base.BaseApplication;
import id.rival.banksampahcipondoh.base.BasePresenter;
import id.rival.banksampahcipondoh.config.General;
import id.rival.banksampahcipondoh.config.SessionManager;
import id.rival.banksampahcipondoh.helper.Utils;
import id.rival.banksampahcipondoh.model.Response;
import id.rival.banksampahcipondoh.network.AppServices;
import id.rival.banksampahcipondoh.view.transaksi.model.PenarikanModel;
import retrofit2.Call;
import retrofit2.Callback;

public class PenarikanPresenter implements BasePresenter<PenarikanInterface> {
    @Inject
    AppServices appServices;
    @Inject
    SessionManager sessionManager;

    private Context context;
    private PenarikanInterface view;

    public PenarikanPresenter(Context context){
        super();
        this.context = context;
        this.sessionManager = new SessionManager(context);
        ((BaseApplication)context.getApplicationContext()).getDeps().inject(this);
    }

    public void savePenarikan(PenarikanModel param){
        view.showLoader();
        Call<Response> call = appServices.saveTransaksi(Utils.modelToJson(param));
        call.enqueue(new Callback<Response>() {
            @Override
            public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
                view.dismissLoader();
                if (response.isSuccessful()){
                    if (response.body().getStatus() == General.API_REQUEST_SUCCESS){
                        view.onSuccessInputPenarikan(response.message());
                    } else {
                        view.onFailed(response.message());
                    }
                }else {
                    view.onNetworkError(response.message());
                }
            }

            @Override
            public void onFailure(Call<Response> call, Throwable t) {
                view.dismissLoader();
                view.onNetworkError(String.valueOf(t));
            }
        });
    }

    @Override
    public void setupView(PenarikanInterface view) {
        this.view = view;
    }

    @Override
    public void clearView() {
        view = null;
    }
}
