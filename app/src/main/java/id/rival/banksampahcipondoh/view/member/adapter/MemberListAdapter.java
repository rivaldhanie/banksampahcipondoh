package id.rival.banksampahcipondoh.view.member.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.List;

import id.rival.banksampahcipondoh.BuildConfig;
import id.rival.banksampahcipondoh.R;
import id.rival.banksampahcipondoh.view.member.model.MemberListModel;
import id.rival.banksampahcipondoh.view.member.ui.MemberDetailActivity;

public class MemberListAdapter extends BaseAdapter {
    private Context context;
    private List<MemberListModel> list;
    private Holder holder;

    public MemberListAdapter(Context context, List<MemberListModel> list){
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView  == null){
            convertView = LayoutInflater.from(context)
                    .inflate(R.layout.member_list_adapter, null);
            holder = new Holder();
            holder.id_nasabah = (TextView) convertView.findViewById(R.id.tx_id_user);
            holder.nama_nasabah = (TextView) convertView.findViewById(R.id.tx_nama_user);
            holder.status = (TextView) convertView.findViewById(R.id.tx_status);
            holder.item = (LinearLayout) convertView.findViewById(R.id.member_list);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }

        MemberListModel memberListModel = list.get(position);
        holder.id_nasabah.setText(memberListModel.getId_nasabah());
        holder.nama_nasabah.setText(memberListModel.getNama_nasabah());
        String sts = memberListModel.getId_login();
        if (sts.equals("101")){
            holder.status.setText("Admin");
        }else {
            holder.status.setText("Nasabah");
        }

        holder.item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, MemberDetailActivity.class);
                intent.putExtra("id_nasabah",memberListModel.getId_nasabah());
                intent.putExtra("nama_nasabah", memberListModel.getNama_nasabah());
                intent.putExtra("alamat_nasabah", memberListModel.getAlamat_nasabah());
                intent.putExtra( "status", memberListModel.getId_login());
                intent.putExtra("telp_nasabah", memberListModel.getTelp_nasabah());
                intent.putExtra("total_tabungan", memberListModel.getTotal_tabungan());
                context.startActivity(intent);
            }
        });

        return convertView;
    }

    private class Holder{
        TextView id_nasabah, nama_nasabah, status;
        LinearLayout item;
    }
}
