package id.rival.banksampahcipondoh.view.main_menu.view;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.loader.content.CursorLoader;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.messaging.FirebaseMessaging;
import com.jaredrummler.android.device.DeviceName;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.rival.banksampahcipondoh.BuildConfig;
import id.rival.banksampahcipondoh.R;
import id.rival.banksampahcipondoh.base.BaseActivity;
import id.rival.banksampahcipondoh.config.General;
import id.rival.banksampahcipondoh.config.SessionManager;
import id.rival.banksampahcipondoh.helper.Utils;
import id.rival.banksampahcipondoh.helper.UtilsSnackbar;
import id.rival.banksampahcipondoh.view.cropper.model.PictureResponse;
import id.rival.banksampahcipondoh.view.cropper.ui.CropPictureActivity;
import id.rival.banksampahcipondoh.view.intro.SplashScreenActivity;
import id.rival.banksampahcipondoh.view.main_menu.adapter.MainMenuAdapter;
import id.rival.banksampahcipondoh.view.main_menu.model.MainMenuModel;
import id.rival.banksampahcipondoh.view.main_menu.process.MainMenuInterface;
import id.rival.banksampahcipondoh.view.main_menu.process.MainMenuPresenter;

public class MainMenuActivity extends BaseActivity implements MainMenuInterface {

    private static final String TAG = MainMenuActivity.class.getSimpleName();

    private TextView title;
    SessionManager mSessionManager;
    @BindView(R.id.layout)
    RelativeLayout layout;
    @BindView(R.id.img_profile)
    ImageView imgProfil;
    @BindView(R.id.txt_nama_user)
    TextView namaUser;
    @BindView(R.id.txt_status)
    TextView statusUser;
    @BindView(R.id.txt_tabungan)
    TextView txTabungan;
    @Inject
    MainMenuPresenter mainMenuPresenter;
    private List<MainMenuModel> menuList = new ArrayList<>();

    int[] menuIcon = new int[]{
            R.drawable.profil_logo,
            R.drawable.member_logo,
            R.drawable.money_logo,
            R.drawable.logo_pemasukan,
//            R.drawable.logo_transaksi,
            R.drawable.logo_notif
    };
    String[] menuTitle = new String[]{
            "Profil",
            "Member List",
            "Tabungan",
            "Pemasukan",
//            "Transaksi",
            "Notifikasi"
    };

    private MainMenuAdapter mainMenuAdapter;
    private ListView listMenu;
    private String status, userId, fcm, tabungan, urlPicture = "";
    private String manufacturer, mTmpGalleryPicturePath;
    Uri file, uri, selectedImage;
    int REQUEST_CAMERA = 0, SELECT_FILE = 1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);
        ButterKnife.bind(this);

        toolbar = findViewById(R.id.toolbar);
        title = toolbar.findViewById(R.id.title);
        mSessionManager = new SessionManager(getApplicationContext());

        getData();

        mainMenuPresenter = new MainMenuPresenter(this);
        mainMenuPresenter.setupView(this);

        mainMenuPresenter.getImage(mSessionManager.getStringFromSP(General.IDNASABAH), "profile");

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setIcon(R.drawable.white_logo);
        title.setText("Bank Sampah Cipondoh Makmur");

        listMenu = findViewById(R.id.list_menu);
        listMenu.setDividerHeight(1);

        initAdapter();
        cekPermission();

        Glide.with(this).load(R.drawable.default_profil).diskCacheStrategy(DiskCacheStrategy.NONE)
                    .skipMemoryCache(true).into(imgProfil);

    }

    private void getToken(String userId) {
        FirebaseMessaging.getInstance().getToken().addOnCompleteListener(new OnCompleteListener<String>() {
            @Override
            public void onComplete(@NonNull Task<String> task) {
                if (!task.isSuccessful()){
                    Log.w(TAG, "FCM registration token failed", task.getException());
                    return;
                }
                // Get new FCM registration token
                fcm = task.getResult();
                mainMenuPresenter.setFcm(userId, fcm);
            }
        });
    }

    @OnClick(R.id.img_profile)
    public void onClickProfile(){
            selectImage();
        }


    private void selectImage(){
        final CharSequence[] items = {"Take Photo", "Use Existing Foto"};
        AlertDialog.Builder builder = new AlertDialog.Builder(MainMenuActivity.this);
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (items[which].equals("Take Photo")){
                    if (checkCameraPermission()){
                        Toast.makeText(MainMenuActivity.this, "Enable camera permission", Toast.LENGTH_SHORT).show();
                        ActivityCompat.requestPermissions(MainMenuActivity.this, new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE},REQUEST_CAMERA);
                    } else {
                        openIntentCamera();
                    }
                } else if (items[which].equals("Use Existing Foto")){
                    if(checkWriteExternalPermission()){
                        openIntentFile();
                    } else {
                        ActivityCompat.requestPermissions(MainMenuActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, SELECT_FILE);
                    }
                }
            }
        });
        builder.show();
    }

    private void openIntentCamera(){
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        file = Uri.fromFile(getOutputMediaFile());
        intent.putExtra(MediaStore.EXTRA_OUTPUT, file);
        startActivityForResult(intent, REQUEST_CAMERA);
    }

    private void openIntentFile(){
        Intent intent = new Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.setType("image/*");
        startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
    }

    private static File getOutputMediaFile(){
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
             Environment.DIRECTORY_PICTURES), "banksampahcipondoh");

        if (!mediaStorageDir.exists()){
            if (!mediaStorageDir.mkdirs()){
                return null;
            }
        }

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        return new File(mediaStorageDir.getPath() + File.separator +
                "IMG_" + timeStamp + ".jpg");

    }

    public boolean checkCameraPermission(){
        return ContextCompat.checkSelfPermission(MainMenuActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED;
    }

    public boolean checkWriteExternalPermission(){
        String permission = Manifest.permission.WRITE_EXTERNAL_STORAGE;
        int res = MainMenuActivity.this.checkCallingOrSelfPermission(permission);
        return (res == PackageManager.PERMISSION_GRANTED);
    }

    private void cekPermission(){
        if (callPermission()){
            getCallPermission();
        }
    }

    private boolean callPermission() {
        return ContextCompat.checkSelfPermission(MainMenuActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED;
    }

    private void getCallPermission(){
        ActivityCompat.requestPermissions(MainMenuActivity.this, new String[]{Manifest.permission.CALL_PHONE},1);
    }

    private void getData(){
        namaUser.setText(mSessionManager.getStringFromSP(General.NAMANASABAH));
        userId = mSessionManager.getStringFromSP(General.IDNASABAH);
        status = mSessionManager.getStringFromSP(General.IDLOGIN);
        if (status.equals("101")){
            statusUser.setText("Admin");
        } else {
            statusUser.setText("Nasabah");
        }
        txTabungan.setText(Utils.formatRupiah(Double.parseDouble(mSessionManager.getStringFromSP(General.TABUNGAN))));
        tabungan = mSessionManager.getStringFromSP(General.TABUNGAN);
        getToken(userId);
    }

    private void remove(String value) {
        for (int i = 0; i < menuList.size(); i++){
            if (menuList.get(i).getMenuName().equals(value)){
                menuList.remove(i);
                mainMenuAdapter.notifyDataSetChanged();
                break;
            }
        }
    }

    private void initAdapter() {
        mainMenuAdapter = new MainMenuAdapter(this, menuList);
        listMenu.setAdapter(mainMenuAdapter);

        for (int i = 0; i < menuIcon.length; i++){
            MainMenuModel mainMenuModel = new MainMenuModel();
            mainMenuModel.setMenuIcon(menuIcon[i]);
            mainMenuModel.setMenuName(menuTitle[i]);
            menuList.add(mainMenuModel);
        }

        if (status.equals("101")){
        } else {
            remove("Member List");
            remove("Transaksi");
        }

        mainMenuAdapter.notifyDataSetChanged();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK){
            if (requestCode == SELECT_FILE)
                if (Build.VERSION.SDK_INT < 19){
                    handleGalleryResult18(data);
                } else {
                    handleGalleryResult19(data);
                }
        } else if (requestCode == 20){
            mainMenuPresenter.getImage(mSessionManager.getStringFromSP(General.IDNASABAH), "profile");
        }
    }

    private void handleGalleryResult18(Intent data){
        String mTmpGalleryPicturePath;
        Uri selectedImage = data.getData();
        mTmpGalleryPicturePath = getRealPathFromURI_API11to18(this, selectedImage);

        if (!mTmpGalleryPicturePath.equals("")){
            if (mTmpGalleryPicturePath != null){
                Intent i = new Intent(this, CropPictureActivity.class);
                i.putExtra("puthImage", mTmpGalleryPicturePath);
                i.putExtra("ket_picture", "profile");
                startActivityForResult(i, 20);
            } else {
                try {
                    InputStream is = getContentResolver().openInputStream(selectedImage);
                    mTmpGalleryPicturePath = selectedImage.getPath();
                    Intent i = new Intent(this, CropPictureActivity.class);
                    i.putExtra("pathImage", mTmpGalleryPicturePath);
                    i.putExtra("ket_picture", "profile");
                    startActivityForResult(i, 20);
                } catch (FileNotFoundException e){
                    e.printStackTrace();
                }
            }
        } else {
            Toast.makeText(this, "Upload failed", Toast.LENGTH_SHORT).show();
        }
    }

    private void handleGalleryResult19(Intent data){
        uri = data.getData();

        DeviceName.with(this).request(new DeviceName.Callback() {
         @Override
         public void onFinished(DeviceName.DeviceInfo info, Exception error) {
             manufacturer = info.manufacturer;
             mTmpGalleryPicturePath = getRealPathFromURI(MainMenuActivity.this, uri);
             if (mTmpGalleryPicturePath == null) {
                 mTmpGalleryPicturePath = getPath(selectedImage);
             }
             Log.d(TAG, "mTmpGalleryPicturePath " + mTmpGalleryPicturePath);
             if (!mTmpGalleryPicturePath.equals("")) {
                 if (mTmpGalleryPicturePath != null) {
//      l && type.equals("1")) {
                     Intent i = new Intent(MainMenuActivity.this, CropPictureActivity.class);
                     i.putExtra("pathImage", mTmpGalleryPicturePath);
                     i.putExtra("ket_picture", "profile");
                     startActivityForResult(i, 20);

                 } else {
                     try {
                         InputStream is = getContentResolver().openInputStream(selectedImage);
                         //mImageView.setImageBitmap(BitmapFactory.decodeStream(is));
                         mTmpGalleryPicturePath = selectedImage.getPath();
                         Log.d(TAG, "mTmpGalleryPicturePath " + mTmpGalleryPicturePath);
//       null && type.equals("1")) {
                         Intent i = new Intent(MainMenuActivity.this, CropPictureActivity.class);
                         i.putExtra("ket_picture", "profile");
                         i.putExtra("pathImage", mTmpGalleryPicturePath);
                         startActivityForResult(i, 20);
//
//      akeText(NupActivity.this, "Upload failed", Toast.LENGTH_SHORT).show();
//
                     } catch (FileNotFoundException e) {
                         e.printStackTrace();
                     }
                 }
             } else {
                 Toast.makeText(MainMenuActivity.this, "Upload failed", Toast.LENGTH_SHORT).show();
             }
         }
      }
                );
    }

    public static String getRealPathFromURI_API11to18(Context context, Uri contentUri){
        String[] proj = {MediaStore.Images.Media.DATA};
        String result = null;

        CursorLoader cursorLoader = new CursorLoader(
                context,
                contentUri, proj, null, null , null);
        Cursor cursor = cursorLoader.loadInBackground();

        if (cursor != null) {
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            result = cursor.getString(column_index);
        }
            return result;
    }

    public String getRealPathFromURI(Context context, Uri contentUri){
        Cursor cursor = null;
        try {
            String[] proj = {MediaStore.Images.Media.DATA};
            cursor = context.getContentResolver().query(contentUri, proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } finally {
            if (cursor != null){
                cursor.close();
            }
        }
    }

    private String getPath(Uri uri){
        String filePath = "";
        try {
            String wholeID = DocumentsContract.getDocumentId(uri);
            String id = wholeID.indexOf(":") > -1 ? wholeID.split(":")[1] : wholeID.indexOf(";") > -1 ? wholeID
                    .split(";")[1] : wholeID;
            String [] column = {MediaStore.Images.Media.DATA};
            String sel = MediaStore.Images.Media._ID + "=?";
            Cursor cursor = getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, column,
                    sel, new String[]{id}, null);
            int columnIndex = cursor.getColumnIndex(column[0]);

            if (cursor.moveToFirst()) {
                filePath = cursor.getString(columnIndex);
            }
            cursor.close();
        }catch (Exception e){
            filePath = "";
        }
        return filePath;
    }


    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.bt_about:
                goAbout();
                return true;
            case R.id.bt_logout:
                doLogout();
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void goAbout() {
        Intent intent = new Intent(MainMenuActivity.this, AboutActivity.class);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_admin_option, menu);
        //getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    private void doLogout() {
        new AlertDialog.Builder(this)
                .setMessage("Anda ingin keluar?")
                .setPositiveButton("Ya", new DialogInterface.OnClickListener(){
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        boolean isAutoStartOn = mSessionManager.isAutoStartOn();
                        mSessionManager.logoutUser();
                        if (isAutoStartOn){
                            mSessionManager.saveAutoStart(true);
                        }
                        Intent intent = new Intent(MainMenuActivity.this, SplashScreenActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                    }
                })
        .setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //Don't Logout
            }
        })
        .show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults ){
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
            if (requestCode == SELECT_FILE){
                openIntentFile();
            }else if (requestCode == REQUEST_CAMERA){
//                openIntentCamera();
            } else {
                Toast.makeText(this, "Membutuhkan perizinan akses", Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void onSuccessGetImage(PictureResponse pictureResponse) {
        urlPicture = pictureResponse.getSavedImage();
        Glide.with(this).load(BuildConfig.BASEURL + urlPicture).diskCacheStrategy(DiskCacheStrategy.NONE)
                .skipMemoryCache(true).into(imgProfil);
    }

    @Override
    public void onSuccessSetFcm(String message) {

    }

    @Override
    public void onFailed(String message) {
        UtilsSnackbar.showSnackbarTypeOne(layout, message);
    }


    @Override
    public void showLoader() {
        viewLoader();
    }

    @Override
    public void dismissLoader() {
        hiddenLoader();
    }

    @Override
    public void onNetworkError(String appErrorMessage) {
        Utils.showSnackbar(layout, appErrorMessage);
    }

    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
                .setMessage("Anda ingin keluar dari aplikasi?")
                .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finishAndRemoveTask();
                    }
                })
                .setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //Do Nothing
                    }
                })
                .show();
    }
}