package id.rival.banksampahcipondoh.view.transaksi.view;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.EditText;

import butterknife.BindView;
import id.rival.banksampahcipondoh.R;
import id.rival.banksampahcipondoh.base.BaseActivity;
import id.rival.banksampahcipondoh.view.transaksi.process.PenarikanInterface;
import id.rival.banksampahcipondoh.view.transaksi.process.PenarikanPresenter;

public class PenarikanActivity extends BaseActivity implements PenarikanInterface {
    PenarikanPresenter penarikanPresenter;
    @BindView(R.id.txt_tanggal)
    EditText txTanggal;
    @BindView(R.id.txt_botol_plastik)
    EditText txBotol;
    @BindView(R.id.txt_kardus)
    EditText txkardus;
    @BindView(R.id.txt_koran)
    EditText txKoran;
    @BindView(R.id.txt_ban)
    EditText txBan;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_penarikan);
    }

    @Override
    public void showLoader() {

    }

    @Override
    public void dismissLoader() {

    }

    @Override
    public void onNetworkError(String appErrorMessage) {

    }

    @Override
    public void onSuccessInputPenarikan(String message) {

    }

    @Override
    public void onFailed(String message) {

    }
}