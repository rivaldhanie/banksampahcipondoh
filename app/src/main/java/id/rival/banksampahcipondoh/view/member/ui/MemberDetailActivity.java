package id.rival.banksampahcipondoh.view.member.ui;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.rival.banksampahcipondoh.R;
import id.rival.banksampahcipondoh.base.BaseActivity;
import id.rival.banksampahcipondoh.helper.Utils;
import id.rival.banksampahcipondoh.view.intro.SplashScreenActivity;
import id.rival.banksampahcipondoh.view.main_menu.view.MainMenuActivity;
import id.rival.banksampahcipondoh.view.member.adapter.MemberHistoryAdapter;
import id.rival.banksampahcipondoh.view.member.adapter.MemberListAdapter;
import id.rival.banksampahcipondoh.view.member.model.MemberHistoryModel;
import id.rival.banksampahcipondoh.view.member.process.MemberDetailInterface;
import id.rival.banksampahcipondoh.view.member.process.MemberDetailPresenter;
import id.rival.banksampahcipondoh.view.transaksi.view.PenarikanActivity;

public class MemberDetailActivity extends BaseActivity implements MemberDetailInterface, View.OnClickListener {
    private List<MemberHistoryModel> list = new ArrayList<>();
    MemberDetailPresenter presenter;
    MemberHistoryAdapter adapter;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.tx_id_member)
    TextView idnsb;
    @BindView(R.id.list_history)
    ListView listView;
    @BindView(R.id.tx_nama_member)
    TextView nmnsb;
    @BindView(R.id.tx_alamat_member)
    TextView almtnsb;
    @BindView(R.id.tx_telp_member)
    TextView telpnsb;
    @BindView(R.id.tx_status_member)
    TextView stsnsb;
    @BindView(R.id.tx_tabungan_member)
    TextView tbngnsb;
    @BindView(R.id.no_data_text)
    TextView txnodata;
    @BindView(R.id.bt_call)
    ImageButton btTelp;
    @BindView(R.id.bt_tarik)
    ImageButton btPenarikan;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_member_detail);
        ButterKnife.bind(this);

        presenter = new MemberDetailPresenter(this);
        presenter.setupView(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Member Detail");

        idnsb.setText(getIntent().getStringExtra("id_nasabah"));
        nmnsb.setText(getIntent().getStringExtra("nama_nasabah"));
        almtnsb.setText(getIntent().getStringExtra("alamat_nasabah"));
        telpnsb.setText(getIntent().getStringExtra("telp_nasabah"));
        String status = getIntent().getStringExtra("status");
        if (status.equals("101")){
            stsnsb.setText("Admin");
        }else {
            stsnsb.setText("Nasabah");
        }
        tbngnsb.setText(Utils.formatRupiah(Double.parseDouble(getIntent().getStringExtra("total_tabungan"))));
        presenter.getHistory(String.valueOf(idnsb.getText()));

        btTelp.setOnClickListener(this);
        btPenarikan.setOnClickListener(this);

    }

    @Override
    public void showLoader() {
        viewLoader();
    }

    @Override
    public void dismissLoader() {
        hiddenLoader();
    }

    @Override
    public void onNetworkError(String appErrorMessage) {
        Toast.makeText(this, appErrorMessage, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onSuccessGetHistoryMember(List<MemberHistoryModel> dataHistory) {
        list.addAll(dataHistory);
        adapter = new MemberHistoryAdapter(this, list);
        listView.setAdapter(adapter);
    }

    @Override
    public void onSuccessNoData(String message) {
        listView.setVisibility(View.GONE);
        txnodata.setVisibility(View.VISIBLE);
    }

    @Override
    public void onFailedGetHistoryMember(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.bt_call:
                doCall();
                break;
            case R.id.bt_tarik:
                Intent intent = new Intent(MemberDetailActivity.this, PenarikanActivity.class);
                startActivity(intent);
                break;
        }
    }

    private void doCall() {
        new AlertDialog.Builder(this)
                .setMessage("Anda ingin Menelpon user ini?")
                .setPositiveButton("Ya", new DialogInterface.OnClickListener(){
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(Intent.ACTION_CALL);
                        intent.setData(Uri.parse("tel:" + telpnsb.getText()));
                        startActivity(intent);
                    }
                })
                .setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //Don't Call
                    }
                })
                .show();
    }
}