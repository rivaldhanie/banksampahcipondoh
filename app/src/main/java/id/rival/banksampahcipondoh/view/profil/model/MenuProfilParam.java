package id.rival.banksampahcipondoh.view.profil.model;

public class MenuProfilParam {
    String id_nasabah;
    String nama_nasabah;
    String alamat_nasabah;
    String telp_nasabah;

    public String getId_nasabah() {
        return id_nasabah;
    }

    public void setId_nasabah(String id_nasabah) {
        this.id_nasabah = id_nasabah;
    }

    public String getNama_nasabah() {
        return nama_nasabah;
    }

    public void setNama_nasabah(String nama_nasabah) {
        this.nama_nasabah = nama_nasabah;
    }

    public String getAlamat_nasabah() {
        return alamat_nasabah;
    }

    public void setAlamat_nasabah(String alamat_nasabah) {
        this.alamat_nasabah = alamat_nasabah;
    }

    public String getTelp_nasabah() {
        return telp_nasabah;
    }

    public void setTelp_nasabah(String telp_nasabah) {
        this.telp_nasabah = telp_nasabah;
    }
}
