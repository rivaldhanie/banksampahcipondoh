package id.rival.banksampahcipondoh.view.main_menu.view;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

import id.rival.banksampahcipondoh.BuildConfig;
import id.rival.banksampahcipondoh.R;

public class AboutActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        TextView versi = (TextView) findViewById(R.id.tx_app_version);
        versi.setText("App Version " + BuildConfig.VERSION_NAME);

    }
}