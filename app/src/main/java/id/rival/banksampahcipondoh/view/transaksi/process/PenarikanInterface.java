package id.rival.banksampahcipondoh.view.transaksi.process;

import id.rival.banksampahcipondoh.base.BaseInterface;

public interface PenarikanInterface extends BaseInterface {
    void onSuccessInputPenarikan(String message);
    void onFailed(String message);
}
