package id.rival.banksampahcipondoh.view.member.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import id.rival.banksampahcipondoh.R;
import id.rival.banksampahcipondoh.view.member.model.MemberHistoryModel;

public class MemberHistoryAdapter extends BaseAdapter {

    private Context context;
    private List<MemberHistoryModel> list;
    private Holder holder;

    public MemberHistoryAdapter (Context context, List<MemberHistoryModel> list){
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null){
            convertView = LayoutInflater.from(context)
                    .inflate(R.layout.member_history_adapter, null);
            holder = new Holder();
            holder.tanggal = (TextView) convertView.findViewById(R.id.tx_tanggal);
            holder.keterangan = (TextView) convertView.findViewById(R.id.tx_keterangan);
            holder.jumlah = (TextView) convertView.findViewById(R.id.tx_jumlah);

            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }

        MemberHistoryModel memberHistoryModel = list.get(position);
        holder.tanggal.setText(memberHistoryModel.getTanggal_record());
        holder.keterangan.setText(memberHistoryModel.getKet_record());
        holder.jumlah.setText(memberHistoryModel.getJumlah_record());

        return convertView;
    }

    private class Holder{
        TextView tanggal, keterangan, jumlah;
    }
}
