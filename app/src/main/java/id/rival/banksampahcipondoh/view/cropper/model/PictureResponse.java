package id.rival.banksampahcipondoh.view.cropper.model;

public class PictureResponse {
    int status;
    String message;
    String savedImage;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getSavedImage() {
        return savedImage;
    }

    public void setSavedImage(String savedImage) {
        this.savedImage = savedImage;
    }
}
