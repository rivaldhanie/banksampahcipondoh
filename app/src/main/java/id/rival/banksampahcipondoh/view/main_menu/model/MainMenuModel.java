package id.rival.banksampahcipondoh.view.main_menu.model;

public class MainMenuModel {
    private int menuIcon;
    private String menuName;

    public int getMenuIcon() {
        return menuIcon;
    }

    public void setMenuIcon(int menuIcon) {
        this.menuIcon = menuIcon;
    }

    public String getMenuName() {
        return menuName;
    }

    public void setMenuName(String menuName) {
        this.menuName = menuName;
    }
}
