package id.rival.banksampahcipondoh.view.login.model;

public class LoginResponse {
    private int status;
    private String message;
    private LoginData loginData;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public LoginData getDataLogin() {
        return loginData;
    }

    public void setLoginData(LoginData loginData) {
        this.loginData =loginData;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
