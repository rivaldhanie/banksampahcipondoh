package id.rival.banksampahcipondoh.view.cropper.ui;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.ByteArrayOutputStream;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.rival.banksampahcipondoh.R;
import id.rival.banksampahcipondoh.base.BaseActivity;
import id.rival.banksampahcipondoh.config.General;
import id.rival.banksampahcipondoh.config.SessionManager;
import id.rival.banksampahcipondoh.view.cropper.model.PictureResponse;
import id.rival.banksampahcipondoh.view.cropper.process.CropPictureInterface;
import id.rival.banksampahcipondoh.view.cropper.process.CropPicturePresenter;

public class CropPictureActivity extends BaseActivity implements CropPictureInterface {
    @Inject
    CropPicturePresenter presenter;
    @BindView(R.id.layout)
    LinearLayout layout;
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    private Bitmap bitmap;
    SessionManager mSessionManager;
    String docType;
    CropImageView cropImageView;
    Button upload;
    String imagePath, id_nasabah, ketPicture;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crop_picture);
        ButterKnife.bind(this);

        presenter = new CropPicturePresenter(this);
        presenter.setupView(this);

        mSessionManager = new SessionManager(getApplicationContext());

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        StrictMode.VmPolicy.Builder newbuilder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(newbuilder.build());

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Crop Foto");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        cropImageView = (CropImageView) findViewById(R.id.CropImageView);
        upload = (Button) findViewById(R.id.bt_upload);

        Intent intent = getIntent();
        imagePath = intent.getStringExtra("pathImage");
        ketPicture = intent.getStringExtra("ket_picture");
        id_nasabah = mSessionManager.getStringFromSP(General.IDNASABAH);
        Log.d("pathImage", imagePath);
        cropImageView.setFixedAspectRatio(true);
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                cropImageView.setAspectRatio(5,6);
            }
        }, 500);
        cropImageView.setImageBitmap(decodeSampledBitmapFromResource(imagePath, 500, 500));

        upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bitmap = cropImageView.getCroppedImage(500, 500);
                if (bitmap != null)
                    cropImageView.setImageBitmap(bitmap);
                cropImageView.setVisibility(View.GONE);
                upload.setVisibility(View.GONE);
                docType = "1";
                String image = getStringImage(bitmap);
                Log.d("image", image);
                presenter.postImage(image, id_nasabah, ketPicture);
                Intent returnIntent = new Intent();
                setResult(Activity.RESULT_OK, returnIntent);
                finish();
            }
        });

    }

    public String getStringImage(Bitmap bmp) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return encodedImage;
    }

    public static Bitmap decodeSampledBitmapFromResource(String resId, int reqWidth, int reqHeight) {
        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(resId, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeFile(resId, options);
    }

    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    @Override
    public void showLoader() {

    }

    @Override
    public void dismissLoader() {

    }

    @Override
    public void onNetworkError(String appErrorMessage) {

    }


    @Override
    public void onSuccessUploadPicture(PictureResponse pictureResponse) {

    }

    @Override
    public void onFailedUploadPicture(String message) {

    }
}