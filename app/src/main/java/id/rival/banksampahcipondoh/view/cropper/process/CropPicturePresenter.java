package id.rival.banksampahcipondoh.view.cropper.process;

import android.content.Context;

import javax.inject.Inject;

import id.rival.banksampahcipondoh.base.BaseApplication;
import id.rival.banksampahcipondoh.base.BasePresenter;
import id.rival.banksampahcipondoh.config.General;
import id.rival.banksampahcipondoh.network.AppServices;
import id.rival.banksampahcipondoh.view.cropper.model.PictureResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CropPicturePresenter implements BasePresenter<CropPictureInterface> {

    @Inject
    AppServices appServices;

    private CropPictureInterface view;
    private Context context;

    public CropPicturePresenter (Context context){
        super();
        ((BaseApplication) context.getApplicationContext()).getDeps().inject(this);
        this.context = context;
    }

    public void postImage(String path_picture, String id_nasabah, String ket_picture){
        view.showLoader();

        Call<PictureResponse> call = appServices.postPicture(path_picture, id_nasabah, ket_picture);
        call.enqueue(new Callback<PictureResponse>() {
            @Override
            public void onResponse(Call<PictureResponse> call, Response<PictureResponse> response) {
                if (response.isSuccessful()){
                    if (response.body().getStatus() == General.API_REQUEST_SUCCESS){
                        view.onSuccessUploadPicture(response.body());
                    }else {
                        view.onFailedUploadPicture(response.body().getMessage());
                    }

                } else {
                    view.onNetworkError("tidak ada jaringan");
                }
            }

            @Override
            public void onFailure(Call<PictureResponse> call, Throwable t) {
                view.onNetworkError(String.valueOf(t));
            }
        });
    }

    @Override
    public void setupView(CropPictureInterface view) {
        this.view = view;
    }

    @Override
    public void clearView() {
        view = null;
    }
}
