package id.rival.banksampahcipondoh.view.profil.ui;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.rival.banksampahcipondoh.R;
import id.rival.banksampahcipondoh.base.BaseActivity;
import id.rival.banksampahcipondoh.config.General;
import id.rival.banksampahcipondoh.config.SessionManager;
import id.rival.banksampahcipondoh.helper.Utils;
import id.rival.banksampahcipondoh.view.main_menu.view.MainMenuActivity;
import id.rival.banksampahcipondoh.view.profil.model.MenuProfilData;
import id.rival.banksampahcipondoh.view.profil.model.MenuProfilParam;
import id.rival.banksampahcipondoh.view.profil.process.MenuProfilInterface;
import id.rival.banksampahcipondoh.view.profil.process.MenuProfilPresenter;

public class MenuProfilActivity extends BaseActivity implements MenuProfilInterface, View.OnClickListener {

    @BindView(R.id.toolbar)
            Toolbar toolbar;
    @BindView(R.id.pr_iduser)
            TextView idUser;
    @BindView(R.id.pr_username)
            TextView username;
    @BindView(R.id.pr_alamat)
            TextView alamat;
    @BindView(R.id.pr_idlog)
            TextView idlog;
    @BindView(R.id.pr_telp)
            TextView telp;
    @BindView(R.id.btsubmit)
            Button submit;
    MenuProfilPresenter menuProfilPresenter;
    String userId;

    SessionManager sessionManager;
    private ListView listmenu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_profil);
        ButterKnife.bind(this);

        menuProfilPresenter = new MenuProfilPresenter(this);
        menuProfilPresenter.setupView(this);
        sessionManager = new SessionManager(getApplicationContext());

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Profil");

        sessionManager = new SessionManager(getApplicationContext());
        idUser.setText(sessionManager.getStringFromSP(General.IDNASABAH));
        username.setText(sessionManager.getStringFromSP(General.NAMANASABAH));
        alamat.setText(sessionManager.getStringFromSP(General.ALAMATNASABAH));
        telp.setText(sessionManager.getStringFromSP(General.TELPNASABAH));
        String stslog = sessionManager.getStringFromSP(General.IDLOGIN);
        if (stslog.equals("101")){
            idlog.setText("Admin");
        } else {
            idlog.setText("Nasabah");
        }

        username.addTextChangedListener(textWatcher);
        alamat.addTextChangedListener(textWatcher);
        telp.addTextChangedListener(textWatcher);

        submit.setOnClickListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            if(validate()){
                submit.setVisibility(View.VISIBLE);
                submit.isClickable();
            }
        }
    };

    @Override
    public void showLoader() {
        viewLoader();
    }

    @Override
    public void dismissLoader() {
        hiddenLoader();
    }

    @Override
    public void onNetworkError(String appErrorMessage) {
        hiddenLoader();
        Toast.makeText(this,appErrorMessage,Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.btsubmit:
                validate();
                MenuProfilParam param = new MenuProfilParam();
                param.setId_nasabah(String.valueOf(idUser.getText()));
                param.setNama_nasabah(String.valueOf(username.getText()));
                param.setAlamat_nasabah(String.valueOf(alamat.getText()));
                param.setTelp_nasabah(String.valueOf(telp.getText()));
                menuProfilPresenter.updateProfile(param);
        }
    }

    private boolean validate() {
        if (username.getText() == sessionManager.getStringFromSP(General.NAMANASABAH)){
            return false;
    } else if(alamat.getText() == sessionManager.getStringFromSP(General.ALAMATNASABAH)){
            return false;
        } else if(telp.getText() == sessionManager.getStringFromSP(General.TELPNASABAH)){
            return false;
        }
        return true;
    }

    @Override
    public void onSuccessUpdateData(String message) {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle("Update Data Berhasil")
                        .setMessage("Data profil anda telah berhasil diupdate")
                                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                    }
                                })
                                        .show();

        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT);
        sessionManager.setStringToSP(General.NAMANASABAH, username.getText().toString());
        sessionManager.setStringToSP(General.ALAMATNASABAH, alamat.getText().toString());
        sessionManager.setStringToSP(General.TELPNASABAH, telp.getText().toString());
        submit.setVisibility(View.GONE);
    }

    @Override
    public void onFailedUpdateData(String message) {
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT);
    }

    @Override
    public void onBackPressed() {
            Intent intent = new Intent(MenuProfilActivity.this, MainMenuActivity.class);
            startActivity(intent);
            finish();
        super.onBackPressed();
    }
}