package id.rival.banksampahcipondoh.view.login.process;

import android.content.Context;
import android.util.Log;

import javax.inject.Inject;

import id.rival.banksampahcipondoh.base.BaseApplication;
import id.rival.banksampahcipondoh.base.BasePresenter;
import id.rival.banksampahcipondoh.config.General;
import id.rival.banksampahcipondoh.config.SessionManager;
import id.rival.banksampahcipondoh.helper.Utils;
import id.rival.banksampahcipondoh.network.AppServices;
import id.rival.banksampahcipondoh.view.login.model.LoginParam;
import id.rival.banksampahcipondoh.view.login.model.LoginResponse;
import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginPresenter implements BasePresenter<LoginInterface> {

    public static final String TAG = LoginPresenter.class.getSimpleName();

    @Inject
    AppServices appServices;
    @Inject
    SessionManager mSessionManager;

    private LoginInterface view;
    private Context context;
    private SessionManager sessionManager;

    public LoginPresenter(Context context){
        super();
        this.context = context;
        this.sessionManager = new SessionManager(context);
        ((BaseApplication) context.getApplicationContext()).getDeps().inject(this);
    }

    public void PostLoginUserSVC(String strId_nasabah, String strPass_nasabah){
        view.showLoader();
        LoginParam param = new LoginParam();
        param.setId_nasabah(strId_nasabah);
        param.setPass_nasabah(strPass_nasabah);
        String aa = Utils.modelToJson(param);
        System.out.println(aa);

        Call<LoginResponse> callLogin = appServices.PostLoginUserSVC(strId_nasabah, strPass_nasabah);
        callLogin.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                view.dismissLoader();
                if (response.isSuccessful()){
                    if (response.body().getStatus() == General.API_REQUEST_SUCCESS){
                        view.loginSuccess(response.body().getDataLogin());
                    }else {
                        view.loginFailed("Email dan/password tidak cocok");
                    }
                } else {
                    view.loginFailed("No Data");
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                view.dismissLoader();
                view.onNetworkError("Jaringan Sedang Bermasalah");
            }
        });
    }


    @Override
    public void setupView(LoginInterface view) {
        this.view = view;
    }

    @Override
    public void clearView() {
        view = null;
    }
}
