package id.rival.banksampahcipondoh.view.member.process;

import java.util.List;

import id.rival.banksampahcipondoh.base.BaseInterface;
import id.rival.banksampahcipondoh.view.member.model.MemberListModel;

public interface MemberListInterface extends BaseInterface {
    void onSuccessGetMemberList(List<MemberListModel> dataNasabah);
    void onFailedGetMemberList(String message);
}
