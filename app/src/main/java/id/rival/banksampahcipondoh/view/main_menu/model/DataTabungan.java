package id.rival.banksampahcipondoh.view.main_menu.model;

public class DataTabungan {
    private String idUser;
    private String totalTabungan;

    public String getIdUser() {
        return idUser;
    }

    public void setIdUser(String idUser) {
        this.idUser = idUser;
    }

    public String getTotalTabungan() {
        return totalTabungan;
    }

    public void setTotalTabungan(String totalTabungan) {
        this.totalTabungan = totalTabungan;
    }
}
