package id.rival.banksampahcipondoh.view.login.model;

public class LoginParam {
    String id_nasabah, pass_nasabah;

    public String getId_nasabah() {
        return id_nasabah;
    }

    public void setId_nasabah(String id_nasabah) {
        this.id_nasabah = id_nasabah;
    }

    public String getPass_nasabah() {
        return pass_nasabah;
    }

    public void setPass_nasabah(String pass_nasabah) {
        this.pass_nasabah = pass_nasabah;
    }
}
