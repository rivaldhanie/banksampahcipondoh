package id.rival.banksampahcipondoh.view.main_menu.model;

public class TabunganResponse {
    private int status;
    private String message;
    private DataTabungan dataTabungan;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataTabungan getDataTabungan() {
        return dataTabungan;
    }

    public void setDataTabungan(DataTabungan dataTabungan) {
        this.dataTabungan = dataTabungan;
    }
}
