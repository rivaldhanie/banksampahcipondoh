package id.rival.banksampahcipondoh.view.main_menu.process;

import android.content.Context;

import javax.inject.Inject;

import id.rival.banksampahcipondoh.base.BaseApplication;
import id.rival.banksampahcipondoh.base.BasePresenter;
import id.rival.banksampahcipondoh.config.General;
import id.rival.banksampahcipondoh.config.SessionManager;
import id.rival.banksampahcipondoh.network.AppServices;
import id.rival.banksampahcipondoh.view.cropper.model.PictureResponse;
import id.rival.banksampahcipondoh.view.main_menu.model.TabunganResponse;
import id.rival.banksampahcipondoh.view.main_menu.model.TabunganParam;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainMenuPresenter implements BasePresenter<MainMenuInterface> {
    @Inject
    AppServices appServices;

    private MainMenuInterface view;
    private Context context;
    private SessionManager mSessionManager;

    public MainMenuPresenter(Context context){
        super();
        ((BaseApplication) context.getApplicationContext()).getDeps().inject(this);
        this.context = context;
        mSessionManager = SessionManager.getInstance(context);
    }

//    public void GetTabunganData(String strUserId){
//        view.showLoader();
//
//        TabunganParam param = new TabunganParam();
//        param.setUserId(strUserId);
//
//        Call<TabunganResponse> call = appServices.GetTabunganData(strUserId);
//        call.enqueue(new Callback<TabunganResponse>() {
//            @Override
//            public void onResponse(Call<TabunganResponse> call, Response<TabunganResponse> response) {
//                view.dismissLoader();
//                if (response.isSuccessful()){
//                    if (response.body().getStatus() == General.API_REQUEST_SUCCESS){
//                        view.onSuccessGetTabungan(response.body().getDataTabungan());
//                    } else {
//                        view.onFailed(response.body().getMessage());
//                    }
//                }
//            }
//
//            @Override
//            public void onFailure(Call<TabunganResponse> call, Throwable t) {
//                view.onNetworkError(String.valueOf(t));
//            }
//        });
//    }

    public void setFcm(String id_nasabah, String fcm){
        view.showLoader();
        Call<id.rival.banksampahcipondoh.model.Response> call = appServices.setFCM(id_nasabah, fcm);
        call.enqueue(new Callback<id.rival.banksampahcipondoh.model.Response>() {
            @Override
            public void onResponse(Call<id.rival.banksampahcipondoh.model.Response> call, Response<id.rival.banksampahcipondoh.model.Response> response) {
                view.dismissLoader();
                if (response.isSuccessful()){
                    if (response.body().getStatus() == General.API_REQUEST_SUCCESS){
                        view.onSuccessSetFcm(response.message());
                    } else {
                        view.onFailed(response.message());
                    }
                } else {
                    view.onNetworkError(response.message());
                }
            }

            @Override
            public void onFailure(Call<id.rival.banksampahcipondoh.model.Response> call, Throwable t) {
                view.dismissLoader();
                view.onNetworkError(String.valueOf(t));
            }
        });
    }

    public void getImage(String id_nasabah, String ket_picture){
        view.showLoader();

        Call<PictureResponse> call = appServices.getPicture(id_nasabah, ket_picture);
        call.enqueue(new Callback<PictureResponse>() {
            @Override
            public void onResponse(Call<PictureResponse> call, Response<PictureResponse> response) {
                view.dismissLoader();
                if (response.isSuccessful()){
                    if (response.body().getStatus() == General.API_REQUEST_SUCCESS){
                        view.onSuccessGetImage(response.body());
                    }else {
                        view.onFailed(response.message());
                    }
                }else {
                    view.onNetworkError(response.message());
                }
            }

            @Override
            public void onFailure(Call<PictureResponse> call, Throwable t) {
                view.onNetworkError(String.valueOf(t));
            }
        });
    }

    @Override
    public void setupView(MainMenuInterface view) {
        this.view = view;
    }

    @Override
    public void clearView() {
        view = null;    }
}
