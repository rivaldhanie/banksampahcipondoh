package id.rival.banksampahcipondoh.view.profil.model;

public class MenuProfilResponse {
    private int status;
    private String message;
    private MenuProfilData menuProfilData;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public MenuProfilData getMenuProfilData() {
        return menuProfilData;
    }

    public void setMenuProfilData(MenuProfilData menuProfilData) {
        this.menuProfilData = menuProfilData;
    }
}
