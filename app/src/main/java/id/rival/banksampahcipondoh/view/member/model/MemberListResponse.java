package id.rival.banksampahcipondoh.view.member.model;

import java.util.List;

public class MemberListResponse {
    int status;
    String message;
    List<MemberListModel> dataNasabah;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<MemberListModel> getDataNasabah() {
        return dataNasabah;
    }

    public void setDataNasabah(List<MemberListModel> dataNasabah) {
        this.dataNasabah = dataNasabah;
    }
}
