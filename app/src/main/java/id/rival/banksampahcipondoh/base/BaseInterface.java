package id.rival.banksampahcipondoh.base;

public interface BaseInterface {

    void showLoader();

    void dismissLoader();

    void onNetworkError(String appErrorMessage);

}
