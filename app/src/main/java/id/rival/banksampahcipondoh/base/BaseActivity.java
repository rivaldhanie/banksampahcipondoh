package id.rival.banksampahcipondoh.base;

import android.content.Context;
import android.graphics.Rect;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import butterknife.BindView;
import id.rival.banksampahcipondoh.R;

public class BaseActivity extends AppCompatActivity {

    private AlertDialog.Builder mDialogBuilder;
    private AlertDialog mAlertDialog;
    @Nullable
    @BindView(R.id.toolbar)
    protected Toolbar toolbar;
    @Nullable
    @BindView(R.id.error_layout)
    protected View viewErrorLayout;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        mDialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_progress, null);
        mDialogBuilder.setView(dialogView);
        mDialogBuilder.setCancelable(false);
        mAlertDialog = mDialogBuilder.create();
    }

    protected void initToolbar(String paramStrTitle){
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(paramStrTitle);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return true;
    }

    protected void viewLoader(){
        mAlertDialog.show();
    }

    protected void hiddenLoader(){
        try {
            if (mAlertDialog != null && mAlertDialog.isShowing()) {
                mAlertDialog.dismiss();
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev){
        if (ev.getAction() == MotionEvent.ACTION_DOWN){
            View v = getCurrentFocus();
            if (v instanceof EditText) {
                Rect outRect = new Rect();
                v.getGlobalVisibleRect(outRect);
                if (!outRect.contains((int) ev.getRawX(), (int) ev.getRawY())) {
                    v.clearFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
        }
        return super.dispatchTouchEvent(ev);
    }

    protected void showAnimateLoader(View pViewParent){
        viewErrorLayout.setVisibility(View.GONE);
//        viewLoaderLayout.setVisibility(View.VISIBLE);
        pViewParent.setVisibility(View.GONE);
    }

    protected void dismissAnimateLoader(){
        viewErrorLayout.setVisibility(View.GONE);
//        viewLoaderLayout.setVisibility(View.GONE);
    }

}
