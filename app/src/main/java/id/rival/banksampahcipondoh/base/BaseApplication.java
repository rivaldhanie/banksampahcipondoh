package id.rival.banksampahcipondoh.base;

import android.app.Application;
import android.content.Context;

import androidx.multidex.MultiDex;

import id.rival.banksampahcipondoh.dagger.ApplicationModule;

import id.rival.banksampahcipondoh.dagger.DaggerDeps;
import id.rival.banksampahcipondoh.dagger.Deps;
import io.realm.Realm;
import io.realm.RealmConfiguration;

public class BaseApplication extends Application {
    private Deps deps;

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
        deps = initDagger(this);
        MultiDex.install(this);
        Realm.init(this);
        RealmConfiguration realmConfiguration =
                new RealmConfiguration.Builder().schemaVersion(2)
                        .deleteRealmIfMigrationNeeded().name("nuansasmarthome.realm").build();
        Realm.setDefaultConfiguration(realmConfiguration);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        MultiDex.install(this);
    }

    public Deps getDeps() {
        return deps;
    }

    protected Deps initDagger(BaseApplication application) {
        return DaggerDeps.builder()
                .applicationModule(new ApplicationModule(application))
                .build();
    }

}
