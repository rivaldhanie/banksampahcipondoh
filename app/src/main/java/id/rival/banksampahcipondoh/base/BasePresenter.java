package id.rival.banksampahcipondoh.base;

public interface BasePresenter<T extends BaseInterface>{

    void setupView(T view);

    void clearView();
}
