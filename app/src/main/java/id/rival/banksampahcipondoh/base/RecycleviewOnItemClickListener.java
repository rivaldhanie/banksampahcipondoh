package id.rival.banksampahcipondoh.base;

public interface RecycleviewOnItemClickListener<T extends Object> {
    void onItemClick(T paramObjModel);
}
